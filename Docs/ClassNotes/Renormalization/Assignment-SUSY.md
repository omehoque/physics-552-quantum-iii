---
jupytext:
  encoding: '# -*- coding: utf-8 -*-'
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.7
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell} ipython3
try: import mmf_setup
except ImportError:
    import sys
    !{sys.executable} -m pip install --user mmf_setup
import mmf_setup;mmf_setup.nbinit()
```

# Hydrogen Atom and 3D Harmonic Oscillator

+++

In this notebook, we will work through the solution of the Hydrogen atom in an analogous procedure to that used to solve for the eigenstates of the harmonic oscillator using the algebraic approach of defining raising and lowering operators.  This complements the approach in your book that uses a series expansion.  Your assignment will be to work through a few of the initial steps in reducing the Hydrogen atom and 3D Harmonic Oscillator problems to a radial Schrödinger equation, then to apply the same chain of arguments that solves the Hydrodgen atom to solve for the spectrum of the 3D isotropic Harmonic Oscillator.

The approach is based on what is called the supersymmetric method (SUSY) and has a similar formal structure to how SUSY is applied to particle physics, but goes back to a paper by Schrödinger:

## References

* [Schrodinger:1940]: "A Method of Determining Quantum-Mechanical Eigenvalues and Eigenfunctions."
* [Cooper:1995]: "Supersymmetry and Quantum Mechanics."  Extensive review about the SUSY method applied to quantum mechanics, including a comprehensive listing of all exactly-solvable problems which use a feature called "shape invariance".

[Schrodinger:1940]: http://www.jstor.org/stable/20490744 'E. Schrödinger, "A Method of Determining Quantum-Mechanical Eigenvalues and Eigenfunctions", Proceedings of the Royal Irish Academy. Section A: Mathematical and Physical Sciences 46, 9--16 (1940)'

[Cooper:1995]: https://doi.org/10.1016/0370-1573(94)00080-M 'Fred Cooper, Avinash Khare, and Uday Sukhatme, "Supersymmetry and Quantum Mechanics", prep 251(5-6), 267--385 (1995) [hep-th/9405029v2](http://arXiv.org/abs/hep-th/9405029v2)'

+++

# Review

+++

## 1D Harmonic Oscillator

+++

To find the eigenstates of the [1D Harmonic Oscillator](https://en.wikipedia.org/wiki/Quantum_harmonic_oscillator) Hamiltonian

$$
  \op{H} = \frac{\op{P}^2}{2m} + \frac{m\omega^2\op{X}^2}{2}, \qquad
  \op{H}\ket{n} = \ket{n}E_n, \qquad
  E_n = \hbar\omega(n + \tfrac{1}{2}),
$$

we noted that it could be factored into a product of raising and lowering operators $\op{a}$:

\begin{gather}
\op{H} = \hbar\omega\left(\op{a}^\dagger\op{a} + \tfrac{1}{2}\right), \qquad
  \op{a} = \frac{1}{r_0\sqrt{2}}\left(\op{X} + \frac{\I}{m\omega}\op{P}\right), \qquad
  r_0 = \sqrt{\frac{\hbar}{m\omega}},\\
  [\op{a}, \op{a}^\dagger] = 1, \qquad
  [\op{a}, \op{H}] = -\hbar\omega\op{a}, \qquad
  [\op{a}^\dagger, \op{H}] = \hbar\omega\op{a}^\dagger.
\end{gather}

This allowed us to conclude that the raising and lowering operators moved between eigenstates:

\begin{gather}
  \op{a}^\dagger\ket{n} = \ket{n+1}\sqrt{n+1}, \qquad
  \op{a}\ket{n} = \ket{n-1}\sqrt{n}.
\end{gather}

Key to the argument was that $\op{H}$ is a sum of non-negative operators $\op{X}^2$ and $\op{P}^2$ with positive coefficients, so $E_n \geq 0$.  This required that there must be a ground state $\ket{0}$ such that $\op{a}\ket{0} = 0$, providing a simple first-order differential equation for the ground state wavefunction, and a constructive procedure for constructing the rest:

\begin{align}
  \op{a}\ket{0} &= 0 & &\implies & \braket{x|\op{a}|0} &\propto \braket{x|\op{X}|0} + \frac{\I}{m\omega}\braket{x|\op{P}|0} = 0 &
  &\implies &
  \left(x + \underbrace{\frac{\hbar}{m\omega}}_{r_0^2}\pdiff{}{x}\right)\psi_0(x) &= 0.
\end{align}

This can be solved by separating variables:

\begin{gather}
  -\frac{x\d{x}}{r_0^2} = \frac{\d{\psi_0}}{\psi_0} = \d{\ln \psi_0}, \qquad
  -\overbrace{\frac{x^2}{2r_0^2}}^{\int_0^{x}x\d{x}/r_0^2} = \ln \frac{\psi_0(x)}{\psi_0(0)},\\
  \begin{aligned}
  \psi_0(x) &= \psi_0(0)e^{-(x/r_0)^2/2} = \frac{1}{\sqrt{r_0\sqrt{\pi}}}e^{-(x/r_0)^2/2},\\
  \psi_{n}(x) &= \frac{\braket{x|\op{a}^\dagger|n-1}}{\sqrt{n}}
              = \frac{\braket{x|\op{X} - \frac{\I}{m\omega}\op{P}|n-1}}{r_0\sqrt{2n}}
              = \frac{1}{\sqrt{2n}}\left(\frac{x}{r_0} - r_0\pdiff{}{x}\right)\psi_{n-1}(x)\\
              &= \frac{1}{\sqrt{2^n n!}}\left(\frac{x}{r_0} - r_0\pdiff{}{x}\right)^{n}\psi_0(x).
   \end{aligned}
\end{gather}

The last form, which solves the recurrence relationship, provides an alternative definition of the Hermite polynomials $H_n(z)$:

$$
  \psi_{n}(x) = \frac{1}{\sqrt{2^n n!}}\frac{1}{\sqrt[4]{\pi r_0^2}}e^{-(x/r_0)^2/2}H_n\left(\frac{x}{r_0}\right), \qquad
  H_n(z) = e^{z^2/2}\left(z - \diff{}{z}\right)^{n}e^{-z^2/2}.
$$

These 1D solutions can be combined to form the complete solution to the 3D isotropic oscillator problem which you will solve at the end of this assignment.

+++

## Hydrogen Atom

+++

A similar strategy, outlined below, completes the solution for the eigenstates of a single electron orbiting a proton – the Hydrogen atom:

$$
  \op{H}\bigl(\vec{\op{R}}^{(e)}, \vec{\op{R}}^{(p)}, \vec{\op{P}}^{(e)}, \vec{\op{P}}^{(e)}\bigr) 
  = \frac{\norm{\vec{\op{P}}^{(e)}}^2}{2m_e} + \frac{\norm{\vec{\op{P}}^{(p)}}^2}{2m_p} - \frac{e^2}{\norm{\vec{\op{R}}^{(e)} - \vec{\op{R}}^{(p)}}}.
$$

### Outline

1. First we will get familiar with the units of the problem, identifying the natural length, energy, and mass scales.
2. Then we will use the techniques from Section 2.3 to reduce this two-body problem to a one-body problem in the center of mass frame.  Quantum mechanically we will show that $\op{H}$ separates:

   $$
     \op{H} = \op{H}_{CM}(\vec{\op{R}}_{CM}, \vec{\op{P}}) + \op{H}_{r}(\vec{\op{r}}, \vec{\op{p}}), \qquad
     \op{H}_{r}(\vec{\op{r}}, \vec{\op{p}}) = \frac{\norm{\vec{\op{p}}}^2}{2m} - \frac{e^2}{\norm{\vec{\op{r}}}}, \qquad
     m = \frac{m_em_p}{m_e+m_p}, \qquad
     [\op{r}_i, \op{p}_{j}] = \I\hbar \delta_{ij},
   $$

   where $m$ is the reduced mass, and $\vec{r}=\vec{R}^{(e)} - \vec{R}^{(p)}$ is the relative coordinate.  This allows us to factor our wavefunction:

   $$
     \psi(\vec{R}^{(e)}, \vec{R}^{(p)}) = \psi_{CM}(\vec{R}_{CM})\psi(\vec{r})
   $$

   where $\psi_{CM}\bigl(\vec{R}_{CM}\bigr)$ will describe the overall motion of the atom, and $\psi(\vec{r})$ will contain all of the information about the structure of the bound states.  Formally, we have used translation invariance to separate the Hamiltonian and factor the wavefunction into eigenstates of the translation operator $\psi_{CM}(\vec{R}_{CM})$ (plane waves) times eigenstates of the relative Hamiltonian.

3. Now that we have a single-particle Hamiltonian in 3D, we use rotational invariance to separate the Hamiltonian and factor the wavefunctions into eigenstates of angular momentum – the spherical harmonics $\braket{\theta, \phi|l,m} = Y^{m}_{l}(\theta, \phi)$ and radial times radial wavefunctions:

   $$
     \ket{Elm} = \ket{El}\otimes\ket{l,m}, \qquad \psi_{Elm}(r, \theta, \phi) = R_{El}(r) Y^{m}_{l}(\theta, \phi).
   $$

   It will turn out that the radial wavefunctions depend on $l$, but not on $m$.

4. We will simplify the form of the radial Schrodinger equation by introducing the radial wavefunction $U_{El}(r) = rR_{El}(r)$.  This will bring the radial equation into the standard form:

   $$
     \frac{-\hbar^2U_{El}''(r)}{2m} + \left(\frac{\hbar^2l(l+1)}{2mr^2} - \frac{e^2}{r}\right)U_{El}(r) = EU_{El}(r),
   $$
   
   subject to the additional boundary condition that $U_{El}(r) = 0$.  This is discussed in detail in section (12.6) for a general radial potential $V(r)$ and applies to our case where $V(r) = e^2/r$.

*At this point, the book considers the series solution to this differential equation, and derives the result that:*

$$
  E_{n} = \frac{-me^4}{2\hbar^2 n^2}, \qquad n \in \{1, 2, 3, \dots\} \tag{13.1.16}
$$

*with additional degeneracies (associated with a hidden symmetry that leads to a conserved [Laplace-Runge-Lenz vector](https://en.wikipedia.org/wiki/Laplace%E2%80%93Runge%E2%80%93Lenz_vector)). We will take a different approach based on the SUSY method.*

+++

# Assignment
## Part A: Units

+++

The Hamiltonian here and in Shankar is expressed in [Gaussian units](https://en.wikipedia.org/wiki/Gaussian_units) where the unit of charge $e = q_e/\sqrt{4\pi\epsilon_0}$ with $q_{e} = 1.602176634\times 10^{-19}$C being the proton [charge in SI units](https://en.wikipedia.org/wiki/Elementary_charge).

1. **Show that the Hamiltonian is dimensionally correct using this definition of $e$.**
2. **Lookup the current best values of the electron mass $m_e$ and proton mass $m_p$.**
3. **Write down all of the physical parameters in this problem, and compute their dimensions.**  I.e. $[\hbar] = M L^2/T$.
4. **Show that the following quantities are dimensionless and compute their values:**

   * $\alpha = \frac{e^2}{\hbar c}$: [Fine-structure constant](https://en.wikipedia.org/wiki/Fine-structure_constant).  This dimensionless quantity sets the strength of the Coulomb interaction potential.
   * $\frac{m_e}{m_p}$: This is the dimensionless mass-ratio.  We shall see that it does not qualitatively affect our results once we introduce the reduced mass $\mu$.

5. **Show that the following quantities have the correct dimension, and compute their numerical values in SI units.**  We shall use these to define the natural scales for distance, time, mass, and charge in our problem, setting each of these to unity.

   * $m = \frac{m_em_p}{m_e+m_p}$: Reduced mass of the proton-electron system.  This will define our unit of mass.  Express the mass of the electron $m_e$ and the mass of the proton $m_p$ in units of [kilo-electronvolts](https://en.wikipedia.org/wiki/Electronvolt) (keV) and giga-electronvolts (GeV) using Einstein's famous relation $E=mc^2$.  These are the nature units for discussing particle physics.  The electron mass $m_ec^2 \approx 511$keV is particularly important in astronomy as photons with this energy typically originate from electron-positron annihilations.  In particular, these photons have been seen coming from the core of our galaxy, where the source of positrons (antimatter) remains a mystery.
   * $r_0 = a_0^* = \frac{\hbar^2}{m e^2}$: [Reduced Bohr radius](https://en.wikipedia.org/wiki/Bohr_radius#%22Reduced%22_Bohr_radius).  This will define our unit of length.  Show that, numerically, this differs from the [Bohr radius](https://en.wikipedia.org/wiki/Bohr_radius), which uses the electron mass $m_e$ in place of the reduced mass $m$, in only the 4th decimal place.  Express $r_0$ in [angstroms](https://en.wikipedia.org/wiki/Angstrom): 1Å $=10^{-10}$m.  This is the closest SI unit to represent the typical distance between atoms in a solid, which is set by the radius of electronic orbits.
   * $E_0 = R_M = \frac{me^4}{2\hbar^2}$: [Reduced Rydberg constant](https://en.wikipedia.org/wiki/Rydberg_constant).  This will define our unit of energy.  Again, show that this differs from the conventional Rydberg constant $R_\infty=$Ry by less than 0.05% due to the difference between the reduced mass and the electron mass $m_e$.  Express this energy in units of [electronvolts](https://en.wikipedia.org/wiki/Electronvolt) 1eV $=1.602176634\times 10^{-19}J$.  The electronvolt (eV) is the natural scale for energies in atomic physics, condensed matter etc. as opposed to the MeV scale which is typical for weak nuclear reactions, and the GeV scale which is typical for strong nuclear interactions.

6. In what follows, choose units so that each of these constant is one: $2m = r_0 = E_0 = 1$.  In this unit system, **what is the numerical value of the following constants:**

   * $\hbar$.
   * One second.
   * One joule.

+++

## Part B: Symmetries and the Center-of-Mass Frame

+++

On the surface, the problem of the Hydrogen atom looks hopelessly complicated.  The wavefunction is a function of 6-variables – the $x$, $y$, and $z$ components each of $\vec{R}^{(e)}$ and $\vec{R}^{(p)}$:

$$
  \Psi(x_e, y_e, z_e, x_p, y_p, z_p).
$$

However, we know that the physics of our universe has many symmetries that lead to conservation laws (classical physics) and commuting operators (quantum physics).  The latter allows us to break the problem in smaller subspace, ultimately reducing the problem to a collection of subproblems each with only a single dimension.

<!--
* Argue that the Hamiltonian is translationally invariant: i.e. that it commutes with the total momentum $\vec{\op{P}} = \vec{\op{P}}^{(e)} + \vec{\op{P}}^{(p)}$. Note there are three components here $\op{P}_x$, $\op{P}_y$, and $\op{P}_z$, all of which commute with the Hamiltonian.

  *Suggestion: Show that*

  \begin{align}
    [\op{T}_{\vec{\epsilon}}^{(e)}, \op{H}(\op{R}^{(e)}, \op{R}^{(p)}, \dots)] &= 
    \op{H}(\op{R}^{(e)} + \vec{\epsilon}, \op{R}^{(p)}, \dots), \\
    [\op{T}_{\vec{\epsilon}}^{(p)}, \op{H}(\op{R}^{(e)}, \op{R}^{(p)}, \dots)] &= 
    \op{H}(\op{R}^{(e)}, \op{R}^{(p)} + \vec{\epsilon}, \dots),\\
    [\op{T}_{\vec{\epsilon}}, \op{H}(\op{R}^{(e)}, \op{R}^{(p)}, \dots)] &= 
    \op{H}(\op{R}^{(e)} + \vec{\epsilon}, \op{R}^{(p)} + \vec{\epsilon}, \dots)
    = \op{H}(\op{R}^{(e)}, \op{R}^{(p)}, \dots),
  \end{align}

  *where*

  $$
    \op{T}^{(e)}_{\vec{\epsilon}} = e^{\vec{\op{P}}^{(e)}\cdot\vec{\epsilon}/\I\hbar}, \qquad
    \op{T}^{(p)}_{\vec{\epsilon}} = e^{\vec{\op{P}}^{(p)}\cdot\vec{\epsilon}/\I\hbar}, \qquad
    \op{T}_{\vec{\epsilon}} = \op{T}^{(p)}_{\vec{\epsilon}} \cdot \op{T}^{(p)}_{\vec{\epsilon}}
                            = e^{\vec{\op{P}}\cdot\vec{\epsilon}/\I\hbar},
  $$

  *are the space-translation operators.*
-->

* The first step is to find a coordinate system in which the center of mass motion separates from the relative motion.  Follow the discussion in section (2.3) and **show that the Hamiltonian separates when expressed in terms of the following operators**:

  $$
    \vec{\op{R}}_{CM} = \frac{m_e\vec{\op{R}}^{(e)} + m_p\vec{\op{R}}^{(p)}}{m_e + m_p}, \qquad
    \vec{\op{r}} = \vec{\op{R}}^{(e)} - \vec{\op{R}}^{(p)},\\
    \vec{\op{P}} = \vec{\op{P}}^{(e)} + \vec{\op{P}}^{(p)}, \qquad
    \vec{\op{p}} = \vec{\op{P}}^{(e)} - \vec{\op{P}}^{(p)}.
  $$
  
  I.e show that:
  
  $$
    \op{H} = \overbrace{\frac{\norm{\vec{\op{P}}}^2}{2M}}^{\op{H}(\vec{\op{P}}, \vec{\op{R}}_{CM})} +
    \overbrace{\frac{\norm{\vec{\op{p}}}^2}{2m} - \frac{e^2}{\norm{\vec{\op{r}}}}}^{\op{H}(\vec{\op{p}}, \vec{\op{r}})}, \\
    [\op{P}_{i}, \op{R}_{j}] = [\op{p}_{i}, \op{r}_{j}] = \I\hbar \delta_{ij}, \qquad
    [\op{P}_{i}, \op{p}_{j}] = [\op{R}_{i}, \op{r}_{j}] = [\op{P}_{i}, \op{r}_{j}] = [\op{p}_{i}, \op{R}_{j}] = 0.
  $$
  
  It is crucial in what follows that the new variables satisfy the standard commutation relationships.  We can now separate the wave-function into two factors, one for the center-of-mass motion, and the other for the relative motion:
  
  $$
    \Psi(x_e, y_e, z_e, x_p, y_p, z_p) = 
    \Psi_{CM}(\vec{\op{R}}_{CM})\psi(\vec{\op{r}})
  $$
  
  We can now worki independently on the three-dimensional problem:
  
  $$
    \left(
      \frac{\norm{\vec{\op{p}}}^2}{2m} - \frac{e^2}{\norm{\vec{\op{r}}}^2}
    \right)\ket{\psi} = \ket{\psi}E.
  $$

+++

## Part C: Spherical Symmetry

+++

1. **Show that the following Hamiltonian commutes with angular momentum.**

  $$
    \op{H} = \frac{\norm{\vec{\op{p}}}^2}{2m} 
    + V(\norm{\vec{\op{r}}})
    = \frac{\op{p}_x^2 + \op{p}_y^2 + \op{p}_z^2}{2m}
    + V\big(\sqrt{\op{x}^2+\op{y}^2+\op{z}^2}\big).
  $$
  
  I.e. Explicitly show that $[\op{H}, \op{L}_z] = 0$ where $\op{L}_z = \op{x}\op{p}_y - \op{p}_y\op{y}$ then argue, then argue that by symmetry, the result must also hold for $\op{L}_x$ and $\op{L}_y$.  The following properties of the commutator may be useful:
  
  $$
    [\op{A}, \op{B}] = -[\op{B}, \op{A}], \qquad
    [\op{A}, \op{B}\op{C}] = \op{B}[\op{A}, \op{C}] + [\op{A}, \op{B}]\op{C},\\
    [\op{x}, \op{p}_x] = [\op{y}, \op{p}_y] = [\op{z}, \op{p}_z],\\
    [\op{x}, \op{y}] = [\op{x}, \op{p}_y] = [\op{p}_x, \op{p}_y] = 0,\\
    \I[\op{p}_x, f(\op{x})] = \hbar f'(\op{x}).
  $$

  Argue that this means that we can find simultaneous eigenstates of $\op{H}$, $\op{L}^2$ and $\op{L}_z$.  I.e. we can express the energy eigenstates in terms of the angular momentum eigenstates (spherical harmonics):
  
  \begin{align}
    \op{L}^2\ket{n;lm} &= \ket{n;lm}\hbar^2 l(l+1),\\
    \op{L}_z\ket{n;lm} &= \ket{n;lm}\hbar m,\\
    \op{H}\ket{n;lm} &= \ket{n;lm}E_{n;lm}
  \end{align}
  
  Here $n$, $l$, and $m$ are said to be good **quantum numbers** describing the eigenstates of the Hamiltonian.  At this point, we do not know if the energy $E_{n;l,m}$ will depend on $l$ and $m$.  We shall see shortly that they do not depend on $m$, so we will be able to write $E_{n;l}$.

2. Express the wavefunction in spherical coordinates:

  $$
    \psi(r,\theta,\phi) = \braket{r,\theta,\phi|\psi}
    = R_{n;lm}(r)Y^{m}_{l}(\theta,\phi).
  $$

  Use the results of Chapter 12, i.e. Eq. (12.5.36) and the spherical form of the Laplacian $\nabla^2$, to **argue that**:

  $$
    \braket{r,\theta,\phi|\norm{\vec{\op{p}}}^2|\psi}
    = -\frac{\hbar^2}{r^2}\pdiff{}{r}\left(r^2\pdiff{}{r}\psi(r,\theta,\phi)\right)
    +\frac{1}{r^2}
    \underbrace{\braket{r,\theta,\phi|\op{L}^2|\psi}}_{\hbar^2l(l+1)\psi(r,\theta,\phi)}.
  $$

  Hence, we need to solve the following radial equation:
  
  $$
    \op{H}_r \rightarrow
    \left(
      \frac{-\hbar^2}{2m}
      \frac{1}{r^2}\pdiff{}{r}\left(r^2\pdiff{}{r}\right)
      \right) 
      + \underbrace{
      \frac{\hbar^2}{2m}\frac{l(l+1)}{r^2}
    + V(r)}_{V_{\text{eff}}(r))}
  $$
  
  Now show that one can simplify the radial equation by introducing the radial wavefunction:
  
  $$
    U_{n;lm}(r) = rR_{n;lm}(r).
  $$
  
  I.e. **show that**

  $$
    (\op{H}_r - E) \frac{U_{n;lm}(r)}{r} = 
    \frac{1}{r}\Bigl(\op{\tilde{H}}_r - E\Bigr) U_{n;lm}(r) = 0.
  $$
  
  I.e., show that where $\op{H}_r$ has a complicated radial form, the extra factor of $1/r$ allows $\op{\tilde{H}}_r$ to simply be a second derivative.  Thus, we can work with the following **radial Schrödinger equation:**
  
  $$
    \Bigl(\op{\tilde{H}}_r - E\Bigr) U_{n;lm}(r) = 
    \Biggl(
      \underbrace{\frac{-\hbar^2}{2m}\diff[2]{}{r}}_{\op{p}_r^2/2m}
      + \frac{\hbar^2}{2m}\frac{l(l+1)}{r^2}
      + V(r) - E
    \Biggr)U_{n;lm}(r) = 0
  $$

  

+++

## Part D: Hydrogen

Now consider the Hydrogen atom as discussed in class:

$$
  V(r) = -\frac{e^2}{r}.
$$

Choose units so that $\hbar = 2m = e^2/2 = 1$.  **Show how to combine these factors to obtain quantities with the following dimensions, with a value of 1 in these units, and express this value in SI units:**  *(I have included the expression for energy as an example.)*
  
* **Energy:** $1 = E_0 = 2m(e/2)^4/\hbar^2 = 2.18\times 10^{-18}$J.  In particle-physics units this is $E_0=13.6$eV.
* **Time:**
* **Distance:**
* **Momentum:**
  
In these units the Hamiltonian has the following form, for which we applied the SUSY method in class and above:
  
$$
  \op{H}^{(l)} = \op{p}_r^2 + \frac{l(l+1)}{\op{r}^2} - \frac{2}{\op{r}}.
$$

+++

## Part E: Isotropic Harmonic Oscillator

+++

Follow a similar strategy to solve for the eigenstates of the isotropic Harmonic Oscillator in 3D:

$$
  V(r) = \frac{m\omega^2}{2} r^2.
$$

Choose units so that $\hbar = 2m = \omega/2 = 1$. **Show how to combine these factors to obtain quantities with the following dimensions, with a value of 1 in these units, and express this value in SI units:**  *(I have included the expression for energy as an example.)*
  
* **Energy:** 
* **Time:**
* **Distance:**
* **Momentum:**
  
In these units the Hamiltonian has the following form, for which we applied the SUSY method in class and above:
  
$$
  \op{H}^{(l)} = \op{p}_r^2 + \frac{l(l+1)}{\op{r}^2} + \op{r}^2.
$$

1. **Guess the form for the superpotential** $W_{l}(r)$ that satisfies the Riccati equation giving the potential:

   $$
     W^2_{l}(r) - W'_{l}(r) = \frac{l(l+1)}{\op{r}^2} + \op{r}^2 - E^{(l)}_0.
   $$
   
   where $E^{(l)}_0$ is the ground state energy. 
   
   *Note: you will probably find several possible superpotentials. Choose one that makes $W'_{l}>0$ positive and $E^{(l)}_0>0$ positive so that the SUSY chain goes in the correct direction.*

2. **Form the chain of super-partners** and find the energy spectrum $E^{(l)}_{s}$ for each Hamiltonian $\op{H}^{(l)}$. 

   *Note: the spectrum here is somewhat different than that in the Hydrogen atom because the Hamiltonian in the chain differ by different constants.  You should find that there are two distinct chains: one for odd $l$ and one for even $l$.  This is reflected in the solution in the book (12.6.53) and the subsequent table.*

3. **Construct the annihilation operator**

  $$
    \op{A}_{l} = W_{l}(\op{r}) + \I \op{p}_r \rightarrow W_l(r) + \diff{}{r}
  $$ 
  
  and solve the resulting differential equation to obtain the ground state wavefunction for $\op{H}^{(l)}$.
4. **Apply the creation operator** 
  $$
    \op{A}_{l}^\dagger = W_{l}(\op{r}) - \I \op{p}_r \rightarrow W_l(r) - \diff{}{r}
  $$ 
  
  to these ground states to construct the complete solution to the problem.
5. **Compare your solution with that in Shankar (12.6.47), (12.6.50), and (12.6.52),** and discuss how the degeneracy is consistent with the solution to Exercise 10.2.3 which solves the same problem by completely separating the problem into 3 1D oscillators:

   \begin{align}
     \op{H} &= 
     \overbrace{\frac{\op{p}_x^2}{2m} + \frac{m\omega^2}{2}\op{x}^2}^{\op{H}_x} + 
     \overbrace{\frac{\op{p}_y^2}{2m} + \frac{m\omega^2}{2}\op{y}^2}^{\op{H}_y} + 
     \overbrace{\frac{\op{p}_z^2}{2m} + \frac{m\omega^2}{2}\op{z}^2}^{\op{H}_z},\\
     \Psi(x, y, z) &= \psi_{n_x}(x)\psi_{n_y}(y)\psi_{n_z}(z), \\
     E &= \hbar \omega(n_x + \tfrac{1}{2} + n_y + \tfrac{1}{2} + n_z + \tfrac{1}{2})
       = \hbar \omega(n_x + n_y + n_z + \tfrac{3}{2})
   \end{align}
       
   where $\psi_{n}(x)$ are the solutions to the 1D problem discussed at the start of this assignment.
   

```{code-cell} ipython3

```
