Bessel Function DVR Bases
=========================
Here we summarize some of the properties of [Bessel function]s we use for the Bessel
function DVR basis.  We make use of the [spherical Bessel functions] of the first kind:

\begin{gather*}
  j_n(r), \qquad
  \left(r^2 \diff[2]{}{r}+ 2r \diff{}{r} + r^2 - n(n+1)\right)j_n'(r) = 0
\end{gather*}





## Roots


[Bessel function]: <https://en.wikipedia.org/wiki/Bessel_function>
[spherical Bessel functions]: <https://en.wikipedia.org/wiki/Bessel_function#Spherical_Bessel_functions>
