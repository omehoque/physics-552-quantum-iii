---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (phys-552-2022)
  language: python
  name: phys-552-2022
---

```{code-cell} ipython3
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

Microscopic Theory
==================
Here is the microscopic theory we use as the basis for renormalizing the Schrödinger
equation.  This is supposed to be "unknown", but we want to make sure it is convergent.

We use our DVR basis which will give good accuracy if we use an analytic potential.  We
take the potential to be:

\begin{gather*}
  V(r) = P(r^2) e^{-(r/r_0)^2/2} + \frac{-\alpha}{\sqrt{r^2 + r_0^2}}
\end{gather*}

where $P(r)$ is some random polynomal, and $r_0$ is the cutoff scale.

```{code-cell} ipython3
from phys_552_2022 import dvr

d = 3
hbar = m = alpha = 1.0
r0 = 0.2

rng = np.random.default_rng(seed=2)

# 3rd order random polynomial
P = (rng.random(3) - 0.5)*2

def V(r):
    return np.polyval(P, (r/r0)**2) * np.exp(-(r/r0)**2/2) - alpha/np.sqrt(r**2 + r0**2)
    
def get_E(R, k_max, l=0):
    basis = dvr.SphericalDVRBasis(R=R, d=d, k_max=k_max)
    r = basis.get_rn(l=l)
    K = hbar**2 / 2 / m * basis.get_K(l=l)
    H = K + np.diag(V(r))
    Es, us = np.linalg.eigh(H)
    return Es, us

R = 10.0
k_max = 100.0
basis = dvr.SphericalDVRBasis(R=R, d=d, k_max=k_max)
rn = basis.get_rn(l=0)
r = np.linspace(0, 2, 1000)
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
ax = axs[0]
ax.plot(r, V(r));
ax.plot(rn, V(rn), '.');
ax.set(xlabel="$r$", ylabel="$V(r)$");
```



```{code-cell} ipython3
from scipy.integrate import solve_bvp
```

```{code-cell} ipython3
k_max = 30.0
Rs = np.linspace(1, 10, 20)
Es = []
for R in Rs:
    _Es, _us = get_E(R, k_max)
    Es.append(_Es[:10])

Es = np.asarray(Es)
fig, ax = plt.subplots()
ax.loglog(Rs, abs(Es - Es[-1]))
```
