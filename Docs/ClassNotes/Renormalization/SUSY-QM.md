---
jupytext:
  encoding: '# -*- coding: utf-8 -*-'
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.7
kernelspec:
  display_name: Python 3 (phys-552-2022)
  language: python
  name: phys-552-2022
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

# Supersymmeytry in Quantum Mechanics

Here we describe an approach is based on what is called the supersymmetric method (SUSY)
applied to quantum mechanics.  This method solves all exactly-sovable problems in
quantum mechanics.  This notebook demonstrates how to use the method, but leaves details
and proofs as an exercise for the reader.  Details are presented in {cite:p}`Cooper:1995`.


## References

* {cite:p}`Cooper:1995`: "Supersymmetry and Quantum Mechanics."  Extensive review about the SUSY method applied to quantum mechanics, including a comprehensive listing of all exactly-solvable problems which use a feature called "shape invariance".


# Summary

We start by choosing units so that $\hbar = 2m = 1$.  Two hamiltonians $\op{H}_1$ and $\op{H}_2$ are considered supersymmetric partners with superpotential $W(x)$ if:

\begin{gather*}
  \begin{aligned}
    \op{H}_1 &= \op{A}^\dagger\op{A} = \op{p}^2 + V(\op{x}) = \op{p}^2 + W^2(\op{x}) - W'(\op{x}),\\
    \op{H}_2 &= \op{A}\op{A}^\dagger = \op{H}_1 + 2W'(\op{x})= \op{p}^2 + W^2(\op{x}) + W'(\op{x}).
  \end{aligned}\\
  \op{A} = W(\op{x}) + \I \op{p}, \qquad \op{H}_1\ket{0} = 0.
\end{gather*}

Note: we have subtracted a constant so that the ground state energy of $\op{H}_1$ is zero.  Explicitly, if $\psi_0(x) = \braket{x|0}$ is the ground-state wavefunction, then the Schrödinger equation gives $-\psi_0''(x) + V(x)\psi_0(x) = 0$.  Since the ground state has no nodes, the potential and superpotentials can be reconstructed from $\psi_0(x)$, solving a [Riccati equation](https://en.wikipedia.org/wiki/Riccati_equation) $V(x) = W^2(x) - W'(x)$:

\begin{gather*}
  V(x) = \frac{\psi_0''(x)}{\psi_0(x)}, \qquad
  W(x) = -\frac{\psi_0'(x)}{\psi_0(x)} = - \diff{}{x}\ln\psi_0(x), \qquad
  \psi_0(x) = A_0 e^{-\int^{x}W(x)\d{x}}.
\end{gather*}

+++ {"cell_style": "center"}

The key feature of these Hamiltonians is that they have the same spectrum (excluding the ground state of $\op{H}_1$) and the eigenstates are related by the operators $\op{A}$ and $\op{A}^\dagger$:

\begin{gather*}
  \begin{aligned}
    \op{H}_1\ket{n}_1 &= \ket{n}_1E_n, \quad n\in\{0, 1, \dots\}, \\
    \op{H}_2\ket{n}_2 &= \ket{n}_2E_n, \quad n\in\{1, \dots\},
  \end{aligned}\\
  \ket{n}_2\sqrt{E_n} = \op{A}\ket{n}_1, \qquad
  \ket{n}_1\sqrt{E_n} = \op{A}^\dagger\ket{n}_2.
\end{gather*}

There are two main uses: 
1. If $\op{H}_1$ is solvable, then the transform immediately gives the solutions for $\op{H}_2$. We shall demonstrate this in our first example using the solution to an infinite square well to get the solution to a $V(x) = \csc^2(x)$ potential.
2. If $\op{H}_1$ and $\op{H}_2$ have the same analytic form (referred to as have a "shape invariant potential" or SIP), then one can construct the entire solution from the ground state(s).  We use this to solve the harmonic oscillator problem where $\op{H}_1$ and $\op{H}_2$ differ by a constant, and the hydrogen atom, where $\op{H}_1$ and $\op{H}_2$ are related by different angular momenta.

+++

## SUSY Hierarchies

+++ {"cell_style": "center"}

The mechanism for taking advantage of shape invariance is to form a hierarchy symmetric Hamiltonians $\op{H}_{s}$, each related by a SUSY pair:

\begin{gather*}
  \op{H}^{(s)} = \op{A}_{s}^\dagger\op{A}_{s} + E_s 
             = \op{A}_{s-1}\op{A}_{s-1}^\dagger + E_{s-1}.
\end{gather*}

For example: consider the spectrum $E_{n}$ of a Hamiltonian $\op{H}^{(0)}$ with ground state $E_0$.  We first let $\op{H}_1 = \op{H}^{(0)} - E_0\mat{1}$, then use the previous process to construct $\op{H}_2$ with ground state energy $E_1 - E_0$, and then $\op{H}^{(1)} = \op{H}_2 + E_0\mat{1}$ which has ground state energy $E_1$.

We now let $\op{H}_1 = \op{H}^{(1)} - E_1\mat{1}$, and form $\op{H}_2$ which has ground state energy $E_2 - E_1$ (since $\op{H}^{(1)}$ has spectrum $E_{n}$ for $n\in\{1, 2, 3, \dots\}$), and let $\op{H}^{(2)} = \op{H}_2 + E_1\mat{1}$.

In this way, we form the following hierarchy of Hamiltonians $\op{H}^{s}$, each of which has the same spectrum starting at $E_s$ of the original Hamiltonian $\op{H} = \op{H}^{(0)}$:

\begin{align}
  \op{H}^{(0)} &= & \{E_0, E_1, E_2, E_3, \dots\}&\\
  \op{H}^{(1)} &= & \{E_1, E_2, E_3, \dots\}&\\
  \op{H}^{(2)} &= & \{E_2, E_3, \dots\}&\\
\end{align}

+++

# Examples

+++

## Infinite Square Well

+++

*Here we show the first two paired Hamiltonians. For more details about the hierarchy, see the following notebook:*

* [SUSY-SquareWell.ipynb](SUSY-SquareWell.ipynb)

For the first example, we consider a solved problem of an infinite square well, and use this to solve the supersymmetric problem:

\begin{gather*}
  \op{H} = \frac{\op{p}^2}{2m} + V(\op{x}), \qquad
  V(x) = \begin{cases}
    0 & 0 < x < L\\
    \infty & \text{otherwise}
  \end{cases}, \\
  \psi_n(x) = \braket{x|n} = \sqrt{\frac{2}{L}}\sin\frac{\pi (1+n) x}{L}, \qquad
  E_n = (1+n)^2\frac{\pi^2\hbar^2}{2mL^2}.
\end{gather*}

### Units
We start by choosing units $\hbar = 2m = L/\pi = 1$.  To reconstruct the dimensionful solution, insert 1 in the following forms to get the correct dimensions:

\begin{gather*}
  [\hbar] = \frac{MD^2}{T}, \qquad [2m] = M, \qquad [L/\pi] = D\\
  1 = \underbrace{\frac{2mL^2}{\pi^2\hbar}}_{\text{time}}
    = \underbrace{\frac{L}{\pi}}_{\text{distance}}
    = \underbrace{\frac{\pi^2\hbar^2}{2mL^2}}_{\text{energy}}
    = \underbrace{\frac{\pi\hbar}{L}}_{\text{momentum}}.
\end{gather*}

### SUSY Relations
Next we form the hamiltonian $\op{H}_1$, subtracting $E_0 = 1$ and construct the superpotential $W(x)$ from the ground state.  This gives the symmetric hamiltonian $\op{H}_2 = \op{H}_1 + 2W'(\op{x})$:

\begin{gather*}
  \op{H}_1 = \op{H} - 1 = \op{p}^2 + V(x) - 1 = \op{A}^\dagger\op{A}, \qquad
  W(x) = -\frac{\psi'_0(x)}{\psi_0(x)} = -\cot{x}, \quad
  E_n = (1+n)^2 - 1 = n(n+2),\\
  W'(x) = \csc^2{x}, \qquad
  \op{H}_2 = \op{p}^2 + 2\csc^2{x} - 1,\\
  \braket{x|n}_2 = \braket{x|\op{A}|n}_1 
  = \frac{1}{\sqrt{E_n}}\left(W(x) + \diff{}{x}\right)\braket{x|n}_1
  = \sqrt{\frac{2}{\pi n(n+1)}}\left(-\cot{x} + \diff{}{x}\right)\sin(1+n) x\\
  \braket{x|n}_2 = \sqrt{\frac{2}{\pi n(n+2)}}\left(
    n\cos\bigl((1+n)x\bigr) - \frac{\sin{nx}}{\sin{x}}\right).
\end{gather*}


+++

Thus, we have a complete solution to the problem of a particle in a rather non-trivial $V(x) = 2\csc^2(x)-1$ potential.

```{code-cell} ipython3
%pylab inline
m = 0.5
hbar = 1
L = np.pi

# Should not change results if we have included dimensions properly
np.random.seed(2)
m, hbar, L = np.random.random(3)+1

N = 100

m_unit = 2*m
x_unit = L/np.pi
p_unit = np.pi*hbar/L
E_unit = p_unit**2/2/m
t_unit = hbar/E_unit

x = np.linspace(0, L, N+2)[1:-1]
dx = np.diff(x).mean()

V1 = - E_unit + 0*x
W = - np.sqrt(E_unit) / np.tan(x/x_unit)
dW = E_unit/np.sin(x/x_unit)**2
V2 = V1 + 2*dW

def get_E1(n):
    return E_unit*n*(1+n)

def get_psi1(x, n):
    x_ = x/x_unit
    return np.sqrt(2/L)*np.sin((1+n)*x_)

def get_psi2(x, n):
    # We add a negative sign so they look nicer.
    x_ = x/x_unit
    return -np.sqrt(2/np.pi/n/(n+2)/x_unit)*(
        n*np.cos((1+n)*x_) - np.sin(n*x_)/np.sin(x_))


# Check orthogonarmality of wavefunctions
n = np.arange(10)
psi1s = get_psi1(x[:, None], n[None, :])
psi2s = get_psi2(x[:, None], n[None, 1:])
assert np.allclose(psi1s.T.conj() @ psi1s * dx, np.eye(len(n)))
assert np.allclose(psi2s.T.conj() @ psi2s * dx, np.eye(len(n)-1))
```

```{code-cell} ipython3
# Nice plot of the symmetric potential
ymin, ymax = -1.1, 14  # Energy ranges
N = 4                  # Number of energy levels
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,10))

Ens = get_E1(np.arange(N))

# Dimensionless versions
x_ = x/x_unit
En_s = Ens/E_unit

# Plot potentials
ax1.plot([0] + x_.tolist() + [L/x_unit],
         [ymax] + (V1/E_unit).tolist() + [ymax], lw=5)
ax2.plot(x/x_unit, V2/E_unit, lw=5)

# Plot energies and wavefunctions
for n, E_ in enumerate(En_s):
    for ax, get_psi in [(ax1, get_psi1),
                        (ax2, get_psi2)]:
        if n == 0 and ax is ax2:
            break

        psi = get_psi(x, n)
        c = f'C{n+1}'
        ax.plot([0, L/x_unit], [E_, E_], c=c, ls=':')
        ax.plot(x_, E_ + psi*np.sqrt(x_unit), ls='-', c=c)

for ax in [ax1, ax2]:
    ax.set(ylim=(ymin, ymax), xlim=(0-dx/x_unit, (L+dx)/x_unit),
           xticks=[0, L/x_unit], xticklabels=['0', 'L'], xlabel='x',
           ylabel=r'$E/(\pi^2\hbar^2/2mL^2)$]',
          )
```

## Harmonic Oscillator
Next we consider the harmonic oscillator in 1D:

\begin{gather*}
  \op{H} = \frac{\op{p}^2}{2m} + \frac{m\omega^2}{2}\op{x}^2.
\end{gather*}

We choose units $\hbar = 2m = \omega/2 = 1$.  To reconstruct the dimensionful solution, insert 1 in the following forms to get the correct dimensions:

\begin{gather*}
  [\hbar] = \frac{MD^2}{T}, \qquad [2m] = M, \qquad [\omega] = \frac{1}{T}\\
  1 = \underbrace{\frac{2}{\omega}}_{\text{time}}
    = \underbrace{\sqrt{\frac{\hbar}{m\omega}}}_{\text{distance}}
    = \underbrace{\frac{\hbar\omega}{2}}_{\text{energy}}
    = \underbrace{\sqrt{\hbar m\omega}}_{\text{momentum}}.
\end{gather*}

Now we form the supersymetric pairs.  We guess $W(x) = x$ which gives $W^2(x) - W'(x) = x^2 - 1$, hence:

\begin{gather*}
  \op{H}_1 = \op{H} - 1 = \op{p}^2 + V(x) - 1 = \op{A}^\dagger\op{A}, \qquad
  W(x) = \op{x}, \qquad
  W'(x) = 1, \\
  \op{H}_2 = \op{H}_{1} + 2 = \op{H} + 1,\\
  \braket{x|n}_2 = \braket{x|\op{A}|n}_1
  = \frac{1}{\sqrt{E_n}}\left(W(x) + \diff{}{x}\right)\braket{x|n}_1
  = \frac{1}{\sqrt{E_n}}\left(x + \diff{}{x}\right)\braket{x|n}_1,\\
  \psi_0(x) = N_0e^{-\int^x W(x)\d{x}} = N_0e^{-\int^x x\d{x}} = N_0e^{-x^2/2}.
\end{gather*}

The argument now is that, since the symmetric parters differ by only a constant, the eigenstates $\ket{n}_2$ and $\ket{n}_1$ are both eigenstates of the original hamiltonian $\op{H} = \op{H}_1 + 1 = \op{H}_2 - 1$.  Thus, the operators $\op{A}$ and $\op{A}^\dagger$ move between different eigenstates of the **same** hamiltonian.  In particular,

\begin{gather*}
  \op{H}_1\ket{n}_1 = \ket{n}_1 E_n, \qquad
  \op{H}_2\ket{n}_2 = \ket{n}_2 E_n,\\
  \op{H}_1\ket{n}_2 = (\op{H}_2 - 2)\ket{n}_2 = \ket{n}_2 (E_n - 2), \quad \implies \quad
  E_{n-1} = E_n - 2, \qquad E_n = 2n,\\
  \ket{n}_2 = \ket{n-1}_1 = \frac{\op{A}\ket{n}_1}{\sqrt{E_n}}, \qquad
  \ket{n}_1 = \frac{\op{A}^\dagger\ket{n}_2}{\sqrt{E_n}} 
            = \frac{\op{A}^\dagger\ket{n-1}_1}{\sqrt{2n}}.
\end{gather*}

The wavefunctions can be constructed by first normalizing the ground state, then using the raising operators:

\begin{gather*}
  \psi_0(x) = \pi^{-1/4}e^{-x^2/2}, \qquad
  \psi_n(x) = \frac{1}{\sqrt{2n}}\left(x - \diff{}{x}\right)\psi_{n-1}(x)
            = \frac{1}{\sqrt{2^nn!}}\left(x - \diff{}{x}\right)^n\psi_{0}(x)
            = \frac{\pi^{-1/4}}{\sqrt{2^n n!}}\left(x - \diff{}{x}\right)^n e^{-x^2/2},\\
  \psi_n(x) = \frac{\pi^{-1/4}}{\sqrt{2^n n!}}e^{-x^2/2}
  \underbrace{\left(e^{x^2/2}\left(x - \diff{}{x}\right)^n e^{-x^2/2}\right)}_{(-1)^nH_n(x)},
\end{gather*}

which are known as the [Hermite functions](https://en.wikipedia.org/wiki/Hermite_polynomials#Hermite_functions), and $H_n(x)$ are the [Hermite polynomials](https://en.wikipedia.org/wiki/Hermite_polynomials):

\begin{gather*}
  H_n(x) = (-1)^n e^{x^2/2}\left(x - \diff{}{x}\right)^n e^{-x^2/2} = (-1)^n e^{x^2}\diff[n]{}{x} e^{-x^2}.
\end{gather*}



+++

Restoring units and expressing the energies of $\op{H} = \op{H}_1 + 1$, we thus have:

\begin{gather*}
  E_n = \frac{\hbar\omega}{2}(2n + 1) = \hbar \omega \left(n + \tfrac{1}{2}\right),  \qquad
  \psi_n(x) = \sqrt[4]{\frac{m\omega}{\pi\hbar}}\frac{(-1)^{n}}{\sqrt{2^n n!}}H_n\left(\frac{x}{a}\right)e^{-x^2/2a^2}, \qquad
  a = \sqrt{\frac{\hbar}{m\omega}}.
\end{gather*}

+++

### Numerical Note

To compute the [Hermite functions](https://en.wikipedia.org/wiki/Hermite_polynomials#Hermite_functions), one can easily derive a two-term recurrence from the action of the raising and lowering operators:

\begin{gather*}
  \braket{x|\op{A} + \op{A}^\dagger|n}_1 = 2x\braket{x|n}_1
  = \sqrt{E_{n+1}}\braket{x|n+1}_1 + \sqrt{E_{n}}\braket{x|n-1}_1,\\
  2x\psi_{n} = \sqrt{2(n+1)}\psi_{n+1} + \sqrt{2n}\psi_{n-1},\\
  \psi_0 = \pi^{-1/4}e^{-x^2/2}, \qquad
  \psi_1 = \sqrt{2}x\psi_0, \qquad
  \psi_{n} = \sqrt{\frac{2}{n}}x\psi_{n-1} - \sqrt{\frac{n-1}{n}}\psi_{n-2}.
\end{gather*}

With a little care (see [this stack-exchange answer](https://scicomp.stackexchange.com/a/30900)), this can be used to generate all wavefunctions to high relative precision. 

```{code-cell} ipython3
%pylab inline
m = 0.5
hbar = 1
w = 2.0

# Should not change results if we have included dimensions properly
np.random.seed(2)
m, hbar, w = np.random.random(3)+1

N = 200

m_unit = 2*m
x_unit = np.sqrt(hbar/m/w)
p_unit = np.sqrt(hbar*m*w)
E_unit = hbar*w/2
t_unit = 2/w

x = np.linspace(-10*x_unit, 10*x_unit, N)
dx = np.diff(x).mean()

x_ = x/x_unit

V1 = E_unit * (x_**2 - 1)
W = np.sqrt(E_unit) * x_
dW = E_unit + 0*x_
V2 = V1 + 2*dW

def get_E1(n):
    return E_unit*2*n

# Memoize for speed with a global cache
_cache = {}

@np.vectorize  # Allows us to pass in n as an array.
def get_psi1(x, n):
    global _cache
    key = (x, n)

    if key not in _cache:
        x_ = x/x_unit
        psi0 = np.pi**(-0.25)*np.exp(-x_**2/2)/np.sqrt(x_unit)
        if n == 0:
            psi = psi0
        elif n == 1:
            psi = np.sqrt(2)*x_*psi0
        else:
            psi = np.sqrt(2/n)*x_*get_psi1(x, n-1) - np.sqrt((n-1)/n)*get_psi1(x, n-2)
        _cache[key] = psi
    return _cache[key]

def get_psi2(x, n):
    return get_psi1(x, n-1)

# Check orthogonarmality of wavefunctions
n = np.arange(10)
psi1s = get_psi1(x[:, None], n[None, :])
assert np.allclose(psi1s.T @ psi1s * dx, np.eye(len(n)))
```

```{code-cell} ipython3
# Nice plot of the symmetric potential
ymin, ymax = -1.1, 14  # Energy ranges
x_min, x_max = -5, 5   # x range
N = 7                  # Number of energy levels
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,10))

Ens = get_E1(np.arange(N))

# Dimensionless versions
x_ = x/x_unit
En_s = Ens/E_unit

# Plot potentials
ax1.plot(x_, V1/E_unit, lw=5)
ax2.plot(x_, V2/E_unit, lw=5)

# Plot energies and wavefunctions
for n, E_ in enumerate(En_s):
    for ax, get_psi in [(ax1, get_psi1),
                        (ax2, get_psi2)]:
        if n == 0 and ax is ax2:
            break

        psi = get_psi(x, n)
        c = f'C{n+1}'
        ax.plot([x_min,x_max], [E_, E_], c=c, ls=':')
        ax.plot(x_, E_ + psi*np.sqrt(x_unit), ls='-', c=c)

for ax in [ax1, ax2]:
    ax.set(ylim=(ymin, ymax), xlim=(x_min, x_max),
           xlabel='x',
           ylabel=r'$2E/\hbar \omega$')
```

## Hydrogen Atom

+++

The final example we will do is the hydrogen atom.  After dealing with the center-of-mass motion, angular momentum, and introducing the radial wavefunction $U_{El}(r)$ we have following 1D problem:

\begin{gather*}
  \psi_{Elm}(\vec{x}) = \overbrace{\frac{U_{El}(r)}{r}}^{R_{El}(r)}Y^{m}_{l}(\phi, \theta), \qquad
  \left(\frac{-\hbar^2}{2m}\diff[2]{}{r} + \frac{\hbar^2 l (l+1)}{2mr^2} - \frac{e^2}{r} - E\right)U_{El}(r) = 0,
\end{gather*}

where $m = m_em_p/(m_e+m_p)$ is the reduced mass, and $e^2 = \alpha \hbar c$ is the square of the proton charge expressed in terms of the fine-structure constant $\alpha \approx 1/137$.

Note that we can think of $U_{El}(r) = \braket{r|n,l}$ if we normalize everything on the interval $r \in [0, \infty)$:

\begin{gather*}
  \int_0^{\infty} r^2\d{r} \abs{R_{El}(r)}^2 = \int_0^{\infty} \d{r} \abs{U_{El}(r)}^2.
\end{gather*}
We start by choosing units so that $\hbar = 2m = e^2/2 =1$.  To reconstruct the dimensionful solution, insert 1 in the following forms to get the correct dimensions:

\begin{gather*}
  [\hbar] = \frac{MD^2}{T}, \qquad [2m] = M, \qquad [e^2] = \frac{MD^3}{T^2}\\
  1 = \underbrace{\frac{2\hbar^3}{me^4}}_{\text{time}}
    = \underbrace{\frac{\hbar^2}{me^2}}_{\text{distance}}
    = \underbrace{\frac{me^4}{2\hbar^2}}_{\text{energy}}
    = \underbrace{\frac{me^2}{\hbar}}_{\text{momentum}}.
\end{gather*}
Let $\op{p} = \op{p}_r$ so that $\braket{r|\op{p}|n,l} = -\I \psi^{(l)}_{n}{}'(r)$.

\begin{gather*}
  \op{H}_{1}^{(l)} = \op{p}^2 + \frac{l(l+1)}{r^2} - \frac{2}{r} - E_0 = \op{H}^{(l)} - E_0.
\end{gather*}

We now guess the form of the superpotential:

\begin{gather*}
  W(r) = \alpha + \frac{\beta}{r}, \qquad
  W'(r) = \frac{-\beta}{r^2}, \qquad
  \frac{l(l+1)}{r^2} - \frac{2}{r} - E_0 = W^2(r) - W'(r) = \frac{\beta(\beta+1)}{r^2} + \frac{2\alpha\beta}{r} + \alpha^2.
\end{gather*}

Thus, $\alpha\beta = -1$, $E_0 = -\alpha^2$, and $\beta \in \{l, -l-1\}$.  We will proceed with the choice of $\beta = -(1+l)$ since we want to also solve for the $l=0$ Hamiltonian.  One can work with $\beta = l$, but then one needs to relabel everything to get the ground state (try it!).

+++

\begin{gather*}
  W(r) = \frac{-(1+l)}{r} + \frac{1}{(1+l)}, \qquad
  W'(r) = -\frac{1+l}{r^2}, \qquad
  V_1(r) = \frac{l(l+1)}{r^2} - \frac{2}{r} + \frac{1}{(1+l)^2},\\ 
  \op{H}_{1}^{(l)} = \op{p}^2 + \frac{l(l+1)}{r^2} - \frac{2}{r} + \frac{1}{(1+l)^2} 
                   = \op{H}^{(l)} + \frac{1}{(1+l)^2},\\
  \braket{r|l, 0}_1 = N^{(l)}_0e^{-\int^r W(r)\d{r}} 
                    = N^{(l)}_0e^{-r/(1+1) + (1+l)\ln{r}}
  = N^{(l)}_0 r^{1+l}e^{-r/(1+l)}, \\
  \int_0^{\infty} r^{2l+2}e^{-2r/(1+l)}\d{r} = (2l+2)!\left(\frac{l}{2}\right)^{l+2},\\
  \braket{r|l, 0}_1 = \frac{1}{\sqrt{(2l+2)!}}\left(\frac{2}{1+l}\right)^{l+3/2}r^{1+l}e^{-r/(1+1)}.
\end{gather*}

+++

The key feature lies in the form of the supersymmetric partner, which is related by a different angular momentum:

\begin{gather*}
  \op{H}_2^{(l)} = \op{H}_{1}^{(l)} + 2 W'(r) = \op{p}^2 + \overbrace{\frac{l(l+1)+2(l+1)}{r^2}}^{(l+2)(l+1)/r^2} - \frac{2}{r} + \frac{1}{(l+1)^2},\\
  \op{H}_2^{(l)} = \op{H}_{1}^{(l+1)} + \frac{1}{(l+1)^2} - \frac{1}{(l+2)^2} = \op{H}_{1}^{(l+1)} + C_{l+1} - C_{l+2},\qquad
  C_{l} = \frac{1}{l^2},\\
  \op{H}_{1}^{(l)}\ket{l,n}_1 = \ket{l,n}_1 E^{(l)}_{n}, \qquad
  \op{H}_{2}^{(l)}\ket{l,n}_2 = \ket{l,n}_2 E^{(l)}_{n}, \qquad
  \op{H}_{1}^{(l+1)}\ket{l,n}_2 = \ket{l,n}_2(E^{(l)}_{n} - C_{l+1} + C_{l+2}).
\end{gather*}

Noting that $C_{l+1} > C_{l+2}$, we see that $\ket{l,n}_2$ has lower energy, so we identify:

\begin{gather*}
  \ket{l, n}_{2} = \ket{l+1, n-1}_1 = \frac{\op{A}^{(l)}\ket{l,n}_1}{\sqrt{E^{(l)}_n}}, \qquad 
  E^{(l+1)}_{n-1} = E^{(l)}_{n} - C_{l+1} + C_{l+2}.
\end{gather*}

Acting with $\op{A}^{(l)}{}^\dagger$ we have:

\begin{gather*}
  \op{A}^{(l)}{}^\dagger\ket{l+1, n-1}_1 
  = \frac{\op{H}^{(l)}_1\ket{l,n}_1}{\sqrt{E^{(l)}_n}} 
  = \ket{l,n}_1\sqrt{E^{(l)}_n}.
\end{gather*}

+++

The labeling in the last step needs some justification, but at this point, we are done.  Using the fact that $E^{(l+n)}_{0}=0$ and manipulating a [telescoping series](https://en.wikipedia.org/wiki/Telescoping_series), we have:

\begin{align}
  E^{(l)}_{n} &= E^{(l+1)}_{n-1} + C_{l+1} - C_{l+2}  
               = E^{(l+2)}_{n-2} + C_{l+2} - C_{l+3} + C_{l+1} - C_{l+2} =\\
              &= E^{(l+2)}_{n-2} + C_{l+1} - C_{l+3} =\\
              &= \vdots\\
              &= E^{(l+n)}_{0} + C_{l+1} - C_{l+n+1}
               = \frac{1}{(l+1)^2} - \frac{1}{(l+n+1)^2},\\
  \psi^{(l)}_{n}(r) &= \braket{r|l,n}_1 
  = \frac{1}{\sqrt{E^{(l)}_{n}}}\braket{r|\op{A}^{(l)}{}^\dagger|l+1,n-1}_1
  = \frac{1}{\sqrt{\frac{1}{(l+1)^2} - \frac{1}{(l+n+1)^2}}}
    \left(\frac{1}{1+l}-\frac{l+1}{r} - \diff{}{r}\right)\psi^{(l+1)}_{n-1}(r).
\end{align}

Simplifying:

\begin{gather*}
  \psi^{(l)}_{n}(r) = \frac{(l+n+1)(l+1)}{\sqrt{(l+n+1)^2-(l+1)^2}}\left(\frac{1}{1+l}-\frac{l+1}{r} - \diff{}{r}\right)\psi^{(l+1)}_{n-1}(r), \qquad
  \psi^{(l)}_{0}(r) = \frac{1}{\sqrt{(2l+2)!}}\left(\frac{2}{1+l}\right)^{l+3/2}r^{l+1}e^{-r/(1+l)}.
\end{gather*}
Returning to the original Hamiltonian $\op{H}^{(l)} = \op{H}^{(l)}_{1} - 1/(l+1)^2$, we note that the last term cancels one piece from the telescoping sum, and we have the complete spectrum for the Hydrogen atom:

\begin{gather*}
  E_{l,n} = -\frac{1}{(l+n+1)^2}, \qquad l, n \in \{0, 1, 2, \cdots\}.
\end{gather*}

+++

To compare with the literature, we now change notations, introducing the **principle quantum number** $\tilde{n} = l+n+1 \in \{1, 2,  3, \dots\}$:

\begin{gather*}
  E_{\tilde{n}} = -\frac{1}{\tilde{n}^2}, \qquad \tilde{n} = l+n+1 \in \{1, 2, 3, \dots\}, 
  \qquad l \in \{0, 1, \dots, \tilde{n}-1\}
\end{gather*}

The wavefunctions are a bit messy, but we have a prescription for calculating them.

\begin{gather*}
  U_{\tilde{n},l}(r) 
  = \psi^{(l)}_{\tilde{n}-l-1}(r)
  = \frac{\tilde{n}(l+1)}{\sqrt{\tilde{n}^2-(l+1)^2}}\left(\frac{1}{1+l}-\frac{l+1}{r} - \diff{}{r}\right)\psi^{(l+1)}_{n-1}(r), \qquad
   = \frac{\tilde{n}l}{\sqrt{\tilde{n}^2 - l^2}}\left(\frac{1}{l}-\frac{l}{r} - \diff{}{r}\right)U_{\tilde{n}l}(r), \\
  U_{\tilde{n},\tilde{n}-1}(r) = \frac{1}{\sqrt{(2\tilde{n})!}}\left(\frac{2}{\tilde{n}}\right)^{\tilde{n}+1/2}r^{\tilde{n}}e^{-r/\tilde{n}}.
\end{gather*}

Thus, the energies are degenerate with $\tilde{n}$ values of $l$ and $2l+1$ values of the $\op{L}_z$ quantum number $m$ for a total degeneracy of

\begin{gather*}
  d = \sum_{l=0}^{\tilde{n}-1} (2l+1) = \tilde{n}^2.
\end{gather*}

```{code-cell} ipython3
%pylab inline
m = 0.5
hbar = 1
e2 = 2

# Should not change results if we have included dimensions properly
np.random.seed(2)
m, hbar, e2 = np.random.random(3)+1

N = 200

m_unit = 2*m
x_unit = hbar**2/m/e2
p_unit = m*e2/2/hbar
E_unit = m*e2**2/2/hbar**2
t_unit = 2*hbar**3/m/e2**2

x = np.linspace(-10*x_unit, 10*x_unit, N)
dx = np.diff(x).mean()

x_ = x/x_unit

############# INCOMPLETE
V1 = E_unit * (x_**2 - 1)
W = np.sqrt(E_unit) * x_
dW = E_unit + 0*x_
V2 = V1 + 2*dW

def get_E1(n):
    return E_unit*2*n

# Memoize for speed with a global cache
_cache = {}

@np.vectorize  # Allows us to pass in n as an array.
def get_psi1(x, n):
    global _cache
    key = (x, n)

    if key not in _cache:
        x_ = x/x_unit
        psi0 = np.pi**(-0.25)*np.exp(-x_**2/2)/np.sqrt(x_unit)
        if n == 0:
            psi = psi0
        elif n == 1:
            psi = np.sqrt(2)*x_*psi0
        else:
            psi = np.sqrt(2/n)*x_*get_psi1(x, n-1) - np.sqrt((n-1)/n)*get_psi1(x, n-2)
        _cache[key] = psi
    return _cache[key]

def get_psi2(x, n):
    return get_psi1(x, n-1)

# Check orthogonarmality of wavefunctions
n = np.arange(10)
psi1s = get_psi1(x[:, None], n[None, :])
assert np.allclose(psi1s.T @ psi1s * dx, np.eye(len(n)))
```

```{code-cell} ipython3

```
